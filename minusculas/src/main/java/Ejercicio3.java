import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejercicio3 {
        public static void main(String[] args) throws Exception {

            String command="java -jar ../Minusculas/out/artifacts/Minusculas_jar";
            List <String> params=Arrays.asList(command.split(" "));
            ProcessBuilder pb=new ProcessBuilder(params);

            try{
                Process minusculas=pb.start();

                Scanner minusculasScanner=new Scanner(minusculas.getInputStream());


                OutputStream os =minusculas.getOutputStream();
                OutputStreamWriter osw=new OutputStreamWriter(os);
                BufferedWriter bw=new BufferedWriter(osw);

                Scanner sc=new Scanner (System.in);

                String linea=sc.nextLine();

                while(! linea.equals("stop")){
                    bw.write(linea);
                    bw.newLine();
                    bw.flush();
                    System.out.println(minusculasScanner.nextLine());
                }


            }catch(IOException ex){
                Logger.getLogger(Ejercicio3.class.getName()).log(Level.SEVERE, (String) null);
            }

        }
    }

