import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

    public class Ejercicio2 {
        public static void main(String[] args) throws Exception {

            String command="java -jar ../Random10/out/artifacts/Random10_jar";
            List <String> params=Arrays.asList(command.split(" "));

            ProcessBuilder pb=new ProcessBuilder(params);

            pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
            pb.redirectInput(ProcessBuilder.Redirect.INHERIT);



            try{
                Process random10=pb.start();
                random10.waitFor();


            }catch(IOException ex){
                Logger.getLogger(Ejercicio2.class.getName()).log(Level.SEVERE, (String) null);
            }

        }
    }

