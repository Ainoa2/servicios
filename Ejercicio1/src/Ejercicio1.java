import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio1 {
    public static void main(String args[]) throws IOException, InterruptedException {

        String command = "ls";
        List<String> params = Arrays.asList(command.split(" "));
        ProcessBuilder pb = new ProcessBuilder(params);
        Process ls = pb.start();

        OutputStream os = ls.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bfw = new BufferedWriter(osw);

        Scanner sc = new Scanner(System.in);

        bfw.write(sc.nextLine());
        bfw.newLine();

        InputStream is;

        int resultado = ls.waitFor();

        if (resultado == 0) {
            is = ls.getInputStream();
        } else {
            is = ls.getErrorStream();
            System.exit(0);
            System.out.println("Error");
        }

        while (sc.hasNext()) {
            if (resultado == 0) {
                System.out.println(sc.nextLine());
            } else {
                System.out.println(sc.nextLine());
            }

        }
    }
}